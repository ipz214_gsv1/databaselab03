-- ������� ��� ������������� �� �������� � �.���
select Nazva, City
from postachalnik
where City Like 'K%'

-- ������� �������� ������� �� ������ ����� ������, �� ����������� �� �����
select Nazva, price * NaSklade As Vartist
from tovar

-- ������� �� ������ �� ������� �������� �� �������� ��
SELECT * from zakaz
WHERE zakaz.date_naznach Between '01.01.2017' And '01.01.2018';

-- ������� ������ ������ �� ������ ������, �������� ��������� ������ ���������� 1 �� 2:
SELECT zakaz_tovar.id_zakaz
FROM zakaz_tovar
WHERE zakaz_tovar.id_tovar In (1,2);

-- ��'� �� �������
select * from sotrudnik
Order by Fname DESC

-- ������� ������ ��� ������ �� ������, ���� ���� �����
-- �� 3 ������ � ������� � ���������� ����� 5 ����.

select tovar.Nazva, tovar.Price, zakaz_tovar.id_zakaz, zakaz_tovar.Kilkist
from tovar JOIN zakaz_tovar ON tovar.id_tovar = zakaz_tovar.id_tovar
where tovar.Price > 3 AND zakaz_tovar.Kilkist > 5

-- 1) ������ ��������� �� ����� ���� ��� ������ ������.
select zakaz_tovar.* from zakaz_tovar
join tovar on zakaz_tovar.id_tovar = tovar.id_tovar
join zakaz on zakaz.id_zakaz = zakaz_tovar.id_zakaz
where zakaz.date_naznach < '07.07.2017' AND tovar.Nazva = '������'

-- 2) ������ ������ ���� ���� ����������� � �������� �������� 
-- � ������� �� ����� �� ����� �� 15 �������.
select * from tovar
where tovar.Price between 10 and 20
AND tovar.NaSklade > 15

-- 3) ������ ��������� ��� ���� �� ��������� ���� ���������.
select * from zakaz
where zakaz.date_naznach is null

-- 4) ������ ������ ������� ������������� ������� � ���������� �������.
select * from tovar
join postachalnik on tovar.id_postav = postachalnik.id_postach
where tovar.NaSklade > 15
AND postachalnik.id_postach = 1

-- 5) ��������� ������� ��� ���������� ������ �� ������� 30 ����, ���� ������������� � ���.
select Sum(zakaz_tovar.Kilkist * tovar.Price) as Vartist from zakaz_tovar
join zakaz on zakaz_tovar.id_zakaz = zakaz.id_zakaz
join tovar on zakaz_tovar.id_tovar = tovar.id_tovar
join postachalnik on tovar.id_postav = postachalnik.id_postach
where postachalnik.id_postach = 2
AND zakaz.date_naznach between '01.07.2017' and '01.08.2017'

-- 6) ��������� ���� ����������� �� ����, ���� ���� ������������� ���������� �� ������� �볺���
select sotrudnik.id_sotrud, zakaz.date_naznach from sotrudnik
join zakaz on zakaz.id_sotrud = sotrudnik.id_sotrud
join klient on zakaz.id_klient = klient.id_klient
where klient.id_klient = 1

-- 7) ������ ������������� �� � ��� � �� ��������� ������ ??????
select * from postachalnik
join tovar on tovar.id_postav = postachalnik.id_postach
where postachalnik.Nazva Like '���%' 

-- 8) ������ �볺��� �� � �� � �������� ������ � ������������ �����.
select klient.Nazva, tovar.Nazva, zakaz.date_naznach from klient
join zakaz on zakaz.id_klient = klient.id_klient
join zakaz_tovar on zakaz_tovar.id_zakaz = zakaz.id_zakaz
join tovar on tovar.id_tovar = zakaz_tovar.id_tovar
where klient.Nazva like '%��%' and zakaz.date_naznach between '2017.07.01' AND '2017.08.01'

-- 9) ������ �����������, �� ��� ����� ������������ �� �������
select * from sotrudnik
where sotrudnik.Fname = '�����'
order by sotrudnik.Lname DESC

-- 10) ������ �볺��� �� ����� e-mail ������������ �� id.
select * from klient
where Tel is not null
order by id_klient


use IlnessHistoryAnalisys


-- ������������� ������

-- 11) ³��������� �����, �� ����� ����

SELECT * FROM IlnessHistory
WHERE VisitDate < '2022-09-10'

-- 12) ������ ��������, �� ���������� � ������ ���, � � ���������� ����� � id 1

SELECT * FROM StudentMedicalCard
JOIN IlnessHistory ON StudentMedicalCard.Id = IlnessHistory.MedicalCardId
JOIN Doctor ON IlnessHistory.DoctorId = 1
WHERE StudentMedicalCard.GroupName LIKE 'IPZ%'

-- 13) ������ ��������� ���������, �� �� ���� ���������� ������

SELECT * FROM IlnessHistory
JOIN StudentMedicalCard ON StudentMedicalCard.Id = IlnessHistory.MedicalCardId
WHERE IlnessHistory.DiagnosisId IS NULL

-- 14) ������ ��������� �������� ��������, ������������ ����� �����������, �� ������ �������� ������ ����� A
-- �� ������� �� � ���������� Բ��

SELECT * FROM IlnessHistory
JOIN StudentMedicalCard ON StudentMedicalCard.Id = IlnessHistory.MedicalCardId
JOIN Doctor ON Doctor.DoctorType = '�����������'
WHERE StudentMedicalCard.Faculty NOT LIKE 'FIKT'
AND StudentMedicalCard.Adress LIKE 'A%'

-- 15) ������ �������� �� ���� �� ������ �� ������� 30 ����, ���� ������ ������

SELECT * FROM StudentMedicalCard
JOIN IlnessHistory ON IlnessHistory.MedicalCardId = StudentMedicalCard.Id
JOIN Doctor ON IlnessHistory.DoctorId = Doctor.Id
WHERE IlnessHistory.VisitDate BETWEEN '01.10.2022' AND '31.10.2022'
AND Doctor.DoctorType = '������'

-- 16) ��������� ���� ������� �� ����, ���� ���� �������� ������� ��������

SELECT Doctor.Id, IlnessHistory.VisitDate FROM IlnessHistory
JOIN StudentMedicalCard ON IlnessHistory.MedicalCardId = StudentMedicalCard.Id
JOIN Doctor ON Doctor.Id = IlnessHistory.DoctorId
WHERE Doctor.Id = 2

-- 17) ������ �������, �� � ���������, �� �� �� �������� ��������

SELECT Doctor.* FROM Doctor
WHERE Doctor.DoctorType = '������'

-- 18) ������ ��������, �� ���������� � ������ ��, � ���� � ����� � ������������ �����


SELECT StudentMedicalCard.* FROM StudentMedicalCard
JOIN IlnessHistory ON IlnessHistory.MedicalCardId = StudentMedicalCard.Id
WHERE IlnessHistory.VisitDate BETWEEN '01.09.2022' AND '31.09.2022'
AND StudentMedicalCard.GroupName LIKE '��%'

-- 19) ������ ����� �� ��'� ������, ������������ �� ��������

SELECT* FROM Doctor
WHERE Doctor.Firstname = 'Oleksiy'
ORDER BY Doctor.Lastname

-- 20) ������ ��������, �� ����� ������ ���� ����������, ������������ �� �d

SELECT * FROM StudentMedicalCard
JOIN IlnessHistory ON IlnessHistory.MedicalCardId = StudentMedicalCard.Id
WHERE IlnessHistory.DiagnosisId IS NOT NULL
ORDER BY StudentMedicalCard.Id


